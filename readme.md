# Introducción
Trabajo primera semana de Desarrollo del lado servidor: NodeJS, Express y MongoDB donde se realizo.

  - Adaptar un template con PUG
  - Incorporar mapa
  - Crear un ambiente MVC
  - Crear la base de una api
## Api
  - /api/bicicletas/ -> Listar bicicletas
  - /api/bicicletas/create -> Crear bicicleta
  - /api/bicicletas/:id/update -> actualizar bicicleta
  - /api/delete -> eliminar bicicleta
