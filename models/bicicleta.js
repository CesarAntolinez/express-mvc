var Bicicleta = function (id, color, modelo, ubicacion) {
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = () => {
    return 'ID : ' + this.id + 'Color : ' + this.color + 'Modelo : ' + this.modelo + 'Ubicación : ' + this.ubicacion;
};

Bicicleta.allBicis = [];
Bicicleta.add = (aBici) => {
    Bicicleta.allBicis.push(aBici);
};

Bicicleta.findByID = (aBiciId) => {
    var bici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (bici)
        return bici;
    else
        throw new Error(`No existe una vicicleta con id ` + aBiciId);
};

Bicicleta.remove = (aBiciId) =>{
    //var bici = Bicicleta.findByID(aBiciId);
    for (var i = 0; i < Bicicleta.allBicis.length; i++)
    {
        if (Bicicleta.allBicis[i].id == aBiciId )
        {
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
};

var a = new Bicicleta(1, 'roja', 'urbano', [4.6060513, -74.1962728]);
var b = new Bicicleta(2, 'azul', 'urbano', [4.6075485, -74.1974316]);

Bicicleta.add(a);
Bicicleta.add(b);

module.exports = Bicicleta;
