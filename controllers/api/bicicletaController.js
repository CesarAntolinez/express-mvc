var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = (req, res) => {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis,
    });
};

exports.bicicleta_create = (req, res) => {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo, [req.body.latitud, req.body.longitud]);
    Bicicleta.add(bici);

    res.status(200).json({
        message: 'create ok',
        bici: bici
    });
};

exports.bicicleta_delate = (req, res) => {
    Bicicleta.remove(req.body.id);
    res.status(204).send();
};

exports.bicicleta_update = (req, res) => {
    var bici = Bicicleta.findByID(req.params.id);
    bici.id         = req.body.id;
    bici.color      = req.body.color;
    bici.modelo     = req.body.modelo;
    bici.ubicacion  = [req.body.latitud, req.body.longitud];
    res.status(204).send();
};
