var mymap = L.map('div-map').setView([4.5993353, -74.1827116], 13);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiY2VzYXJhbnRvbGluZXoiLCJhIjoiY2tlNGpteTBjMHR6YjJ0c3p3djBwbGptNSJ9.WgLI3xF9sjmI83ZsmTjOSg'
}).addTo(mymap);
//L.marker([4.6060513, -74.1962728]).addTo(mymap);
//L.marker([4.6075485, -74.1974316]).addTo(mymap);
/*var polygon = L.polygon([
    [4.6060513, -74.1962728],
    [4.6075485, -74.1974316]
]).addTo(mymap);*/
$.ajax({
    dataType: 'json',
    url: 'api/bicicletas',
    success: (response) =>{
        console.log(response);
        response.bicicletas.forEach((item) => {
            L.marker(item.ubicacion, {title: item.id}).addTo(mymap);
        });
    }
});
